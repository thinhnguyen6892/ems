package edu.hcmus.solutions.ems.web.rest;

import edu.hcmus.solutions.ems.api.DepartmentService;
import edu.hcmus.solutions.ems.api.dto.DepartmentDto;
import edu.hcmus.solutions.ems.domain.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/Departments")
public class DepartmentEndpoint {

    @Autowired
    private DepartmentService DepartmentService;

    @GetMapping
    public Iterable getAllDepartments() {
        return DepartmentService.findAll();
    }

    /**
     */
    @RequestMapping(value = "/create",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> create(@Valid @RequestBody DepartmentDto DepartmentDto, HttpServletRequest request) {
        Department Department = DepartmentService.createDepartment(DepartmentDto);
        if(Department != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }

}
