package edu.hcmus.solutions.ems.web.rest;

import edu.hcmus.solutions.ems.api.StudentService;
import edu.hcmus.solutions.ems.api.dto.StudentDto;
import edu.hcmus.solutions.ems.api.exception.BusinessLogicException;
import edu.hcmus.solutions.ems.domain.model.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/students")
public class StudentEndpoint {

    private final Logger log = LoggerFactory.getLogger(StudentEndpoint.class);
    @Autowired
    private StudentService studentService;

    @GetMapping
    public Iterable getAllStudents() {
        return studentService.findAll();
    }

    /**
     */
    @RequestMapping(value = "/create",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> create(@Valid @RequestBody StudentDto studentDto, HttpServletRequest request) {
        Student student = null;
        try {
            student = studentService.createStudent(studentDto);
            if(student != null) {
                return new ResponseEntity<>(HttpStatus.CREATED);
            }
        } catch (BusinessLogicException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }

    /**
     */
    @RequestMapping(value = "/score",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> adjustScore(@Valid @RequestBody StudentDto studentDto, HttpServletRequest request) {
        try {
            return ResponseEntity.ok(studentService.adjustScore(studentDto));
        } catch (BusinessLogicException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity search(@RequestParam(value = "id") String id, @RequestParam(value = "idNum") String idNum) {
        try {
            return ResponseEntity.ok(studentService.findByIdAndIdNumber(id, idNum));
        } catch (BusinessLogicException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
