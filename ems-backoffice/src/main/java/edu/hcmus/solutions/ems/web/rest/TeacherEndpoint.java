package edu.hcmus.solutions.ems.web.rest;

import edu.hcmus.solutions.ems.api.TeacherService;
import edu.hcmus.solutions.ems.api.dto.TeacherDto;
import edu.hcmus.solutions.ems.domain.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/teachers")
public class TeacherEndpoint {

    @Autowired
    private TeacherService TeacherService;

    @GetMapping
    public Iterable getAllTeachers() {
        return TeacherService.findAll();
    }

    /**
     */
    @RequestMapping(value = "/create",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> create(@Valid @RequestBody TeacherDto TeacherDto, HttpServletRequest request) {
        Teacher Teacher = TeacherService.createTeacher(TeacherDto);
        if(Teacher != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = "/update",
            method = RequestMethod.PUT,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> update(@Valid @RequestBody TeacherDto TeacherDto, HttpServletRequest request) {
        Teacher Teacher = TeacherService.updateTeacher(TeacherDto);
        if(Teacher != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@Valid @RequestBody TeacherDto TeacherDto, HttpServletRequest request) {
        TeacherService.deleteTeacher(TeacherDto);
        return new ResponseEntity(HttpStatus.OK);
    }


}
