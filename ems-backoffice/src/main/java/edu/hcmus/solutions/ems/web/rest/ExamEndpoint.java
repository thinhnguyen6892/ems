package edu.hcmus.solutions.ems.web.rest;

import edu.hcmus.solutions.ems.api.ExamService;
import edu.hcmus.solutions.ems.api.dto.ExamDto;
import edu.hcmus.solutions.ems.domain.model.Exam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/exams")
public class ExamEndpoint {

    @Autowired
    private ExamService ExamService;

    @GetMapping
    public Iterable getAllExams() {
        return ExamService.findAll();
    }

    /**
     */
    @RequestMapping(value = "/create",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> create(@Valid @RequestBody ExamDto ExamDto, HttpServletRequest request) {
        Exam Exam = ExamService.createExam(ExamDto);
        if(Exam != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = "/update",
            method = RequestMethod.PUT,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> update(@Valid @RequestBody ExamDto ExamDto, HttpServletRequest request) {
        Exam Exam = ExamService.updateExam(ExamDto);
        if(Exam != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@Valid @RequestBody ExamDto ExamDto, HttpServletRequest request) {
        ExamService.deleteExam(ExamDto);
        return new ResponseEntity(HttpStatus.OK);
    }


}
