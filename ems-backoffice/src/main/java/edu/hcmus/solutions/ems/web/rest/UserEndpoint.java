package edu.hcmus.solutions.ems.web.rest;

import edu.hcmus.solutions.ems.api.UserService;
import edu.hcmus.solutions.ems.security.web.rest.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.expression.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserEndpoint {


    @Autowired
    private UserService userService;

    @GetMapping
    public List getAllUsers() {
        return userService.findAll();
    }
}
