package edu.hcmus.solutions.ems.view.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TeacherController {
    @RequestMapping(value="/teacher")
    public String index(){
        return "admin/teacher/index";
    }
}
