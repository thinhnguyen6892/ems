package edu.hcmus.solutions.ems.web.rest;

import edu.hcmus.solutions.ems.api.CalendarService;
import edu.hcmus.solutions.ems.api.dto.CalendarDto;
import edu.hcmus.solutions.ems.domain.model.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/calendars")
public class CalendarEndpoint {

    @Autowired
    private CalendarService CalendarService;

    @GetMapping
    public Iterable getAllCalendars() {
        return CalendarService.findAll();
    }

    /**
     */
    @RequestMapping(value = "/create",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> create(@Valid @RequestBody CalendarDto CalendarDto, HttpServletRequest request) {
        Calendar Calendar = CalendarService.createCalendar(CalendarDto);
        if(Calendar != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = "/update",
            method = RequestMethod.PUT,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> update(@Valid @RequestBody CalendarDto CalendarDto, HttpServletRequest request) {
        Calendar Calendar = CalendarService.updateCalendar(CalendarDto);
        if(Calendar != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@Valid @RequestBody CalendarDto CalendarDto, HttpServletRequest request) {
        CalendarService.deleteCalendar(CalendarDto);
        return new ResponseEntity(HttpStatus.OK);
    }


}
