package edu.hcmus.solutions.ems.web.rest;

import edu.hcmus.solutions.ems.api.RoomService;
import edu.hcmus.solutions.ems.api.dto.RoomDto;
import edu.hcmus.solutions.ems.domain.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/rooms")
public class RoomEndpoint {

    @Autowired
    private RoomService RoomService;

    @GetMapping
    public Iterable getAllRooms() {
        return RoomService.findAll();
    }

    /**
     */
    @RequestMapping(value = "/create",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> create(@Valid @RequestBody RoomDto RoomDto, HttpServletRequest request) {
        Room Room = RoomService.createRoom(RoomDto);
        if(Room != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = "/update",
            method = RequestMethod.PUT,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> update(@Valid @RequestBody RoomDto RoomDto, HttpServletRequest request) {
        Room Room = RoomService.updateRoom(RoomDto);
        if(Room != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@Valid @RequestBody RoomDto RoomDto, HttpServletRequest request) {
        RoomService.deleteRoom(RoomDto);
        return new ResponseEntity(HttpStatus.OK);
    }


}
