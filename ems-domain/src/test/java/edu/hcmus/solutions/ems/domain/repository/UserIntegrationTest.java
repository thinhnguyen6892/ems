package edu.hcmus.solutions.ems.domain.repository;

import edu.hcmus.solutions.ems.domain.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserIntegrationTest {

    //This used for create the test data.
    @Autowired
    private TestEntityManager entityManager;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private SupportRepository supportRepository;


    private Role superAdminRole;

    private Teacher teacher;

    @Before
    public void before(){
        entityManager.clear();
        superAdminRole = new Role();
        superAdminRole.setName("SUPER_ADMIN");
        roleRepository.saveAndFlush(superAdminRole);
        superAdminRole = roleRepository.findAll().get(0);

        Department department = new Department();
        department.setName("Information Technology Facility");
        departmentRepository.saveAndFlush(department);
        List<Department> departments = departmentRepository.findAll();
        assertThat(departments.size()).isGreaterThan(1);
        department = departments.get(1);

        teacher = new Teacher();
        teacher.setFirstName("Admin");
        teacher.setLastName("Super");
        teacher.setDepartment(department);
        teacher.setAddress("227 Nguyen Van Cu, District 5, Ho Chi Minh City, Vietnam 70000");
        teacher.setPhone("0838354266");
        teacherRepository.saveAndFlush(teacher);
        List<Teacher> teachers = teacherRepository.findAll();
        assertThat(teachers.size()).isGreaterThan(1);
        teacher = teachers.get(1);
    }

    @Test
    public void test_register_new_user() {
        User superAdmin = new User();
        superAdmin.setLogin("admin");
        superAdmin.setPassword(passwordEncoder.encode("admin"));
        superAdmin.addRole(superAdminRole);
        superAdmin.setTeacher(teacher);
        userRepository.saveAndFlush(superAdmin);
        List<User> users = userRepository.findAll();
        assertThat(users.size()).isEqualTo(1);
        superAdmin = users.get(0);
        assertThat(superAdmin.getLogin()).isEqualTo("admin");
    }

    @Test
    public void test_support_function() {
        Support support = new Support();
        support.setName("Thinh");
        support.setQuestion("Who you are");
        support.setDateQuestion(Calendar.getInstance());
        support.setPhone("0838354266");
        support.setAttachment("Nani");
        support.setAnswer("Here are my answers");
        support.setDateAnswer(Calendar.getInstance());
        support.setIdNumber("Unknown");
        support.setTeacher(teacher);
        supportRepository.saveAndFlush(support);
        List<Support> supports = supportRepository.findAll();
        assertThat(supports.size()).isEqualTo(1);
        support = supports.get(0);
        assertThat(support.getName()).isEqualTo("Thinh");

    }
}
