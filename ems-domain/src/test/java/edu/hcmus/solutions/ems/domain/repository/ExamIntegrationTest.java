package edu.hcmus.solutions.ems.domain.repository;


import edu.hcmus.solutions.ems.domain.model.*;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class ExamIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;


    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private ExamRepository examRepository;

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TranscriptRepository transcriptRepository;

    private Teacher teacher;
    private Calendar calendar;

    @Before
    public void before(){
        entityManager.clear();
        Department department = new Department();
        department.setName("Information Technology Facility");
        departmentRepository.saveAndFlush(department);

        teacher = new Teacher();
        teacher.setFirstName("Admin");
        teacher.setLastName("Super");
        teacher.setDepartment(department);
        teacher.setAddress("227 Nguyen Van Cu, District 5, Ho Chi Minh City, Vietnam 70000");
        teacher.setPhone("0838354266");
        teacherRepository.saveAndFlush(teacher);

        Exam exam = new Exam();
        exam.setYear(new Date());
        exam.setGroupId("A");
        examRepository.saveAndFlush(exam);

        calendar = new Calendar();
        calendar.setSubject("Math");
        calendar.setDate(new Date());
        calendar.setStartTime(new Date());
        calendar.setEndTime(new Date());
        calendar.setShift(1);
        calendar.setExam(exam);
        calendarRepository.saveAndFlush(calendar);
    }

    @Test
    public void test_create_student_transcript() {
        Room room = new Room();
        room.setName("C33");
        room.setLocation("227 Nguyen Van Cu, District 5, Ho Chi Minh City, Vietnam 70000");
        room.setCalendar(calendar);
        roomRepository.saveAndFlush(room);
        room = roomRepository.findAll().get(0);
        Student student = new Student();
        student.setAddress("227 Nguyen Van Cu, District 5, Ho Chi Minh City, Vietnam 70000");
        student.setBirthDate(new Date());
        student.setFirstName("Thinh");
        student.setLastName("Nguyen");
        student.setIdNumber("CMND");
        student.setRegion(1);
        student.setPhone("4564987987");
        student.addRoom(room);
        studentRepository.saveAndFlush(student);
        List<Student> students = studentRepository.findAll();
        assertThat(students.size()).isEqualTo(1);
        student = students.get(0);

        Transcript transcript = transcriptRepository.findTranscriptByRoomIdAndAndStudentId(room.getId(), student.getId());
        assertThat(transcript).isNotNull();
        transcript.setScore(6.5f);
        transcriptRepository.saveAndFlush(transcript);

        Transcript result = transcriptRepository.findTranscriptByRoomIdAndAndStudentId(room.getId(), student.getId());
        assertThat(result).isNotNull();
        assertThat(result.getScore()).isEqualTo(6.5f);
    }

    @Test
    public void test_teacher_has_assignment() {

        Assignment assignment = new Assignment();
        assignment.setSalary(2000d);
        assignment.setPosition("Giam Thi Chinh");
        assignment.setTeacher(teacher);

        Room room = new Room();
        room.setName("C33");
        room.setLocation("227 Nguyen Van Cu, District 5, Ho Chi Minh City, Vietnam 70000");
        room.setCalendar(calendar);
        List<Assignment> assignments = room.getAssignments() != null ? room.getAssignments() : Lists.newArrayList();
        assignments.add(assignment);
        room.setAssignments(assignments);
        roomRepository.saveAndFlush(room);
        List<Room> rooms = roomRepository.findAll();
        assertThat(rooms.size()).isEqualTo(1);
        room = rooms.get(0);
        assertThat(room.getAssignments().size()).isEqualTo(1);
        assertThat(room.getAssignments().get(0).getSalary()).isEqualTo(2000);
    }
}
