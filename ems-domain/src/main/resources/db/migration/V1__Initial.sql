
-- -----------------------------------------------------
-- Table `department`
-- -----------------------------------------------------
DROP TABLE IF EXISTS department;
CREATE TABLE department(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(45) NULL,
  PRIMARY KEY (id)
)
;


-- -----------------------------------------------------
-- Table `teacher`
-- -----------------------------------------------------
DROP TABLE IF EXISTS teacher;
CREATE TABLE teacher(
  id int NOT NULL AUTO_INCREMENT,
  first_name varchar(45) NULL,
  last_name varchar(45) NULL,
  phone varchar(50) NULL,
  address varchar(100) NULL,
  department_id int NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_teacher_department1
    FOREIGN KEY (department_id)
    REFERENCES department (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
;



-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS user;
CREATE TABLE user(
  login varchar(100) NOT NULL,
  password varchar(255) NOT NULL,
  enabled tinyint NULL,
  teacher_id int NOT NULL,
  PRIMARY KEY (login),
  CONSTRAINT fk_user_teacher1_idx
  FOREIGN KEY (teacher_id)
  REFERENCES teacher (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
;


-- -----------------------------------------------------
-- Table `role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS role;
CREATE TABLE role(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(45) NULL DEFAULT NULL,
  PRIMARY KEY (id)
)

;


-- -----------------------------------------------------
-- Table `user_role_assignment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS user_role_assignment;
CREATE TABLE user_role_assignment(
  user_id varchar(100) NOT NULL,
  role_id int NOT NULL,
  PRIMARY KEY (
    user_id,
    role_id
  ),
  CONSTRAINT fk_user_role_assignment_role1
    FOREIGN KEY (role_id)
    REFERENCES role (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fk_user_role_assignment_user
    FOREIGN KEY (user_id)
    REFERENCES user (login) ON DELETE NO ACTION ON UPDATE NO ACTION
)

;


-- -----------------------------------------------------
-- Table `student`
-- -----------------------------------------------------
DROP TABLE IF EXISTS student;
CREATE TABLE student(
  id varchar(45) NOT NULL,
  first_name varchar(100) NULL,
  last_name varchar(100) NULL,
  birth_date date NULL,
  place_of_birth varchar(100) NULL,
  address varchar(1000) NULL,
  phone varchar(50) NULL,
  region int NULL,
  id_number varchar(45) NULL,
  PRIMARY KEY (id)
)
;


-- -----------------------------------------------------
-- Table `exam`
-- -----------------------------------------------------
DROP TABLE IF EXISTS exam;
CREATE TABLE exam(
  id int NOT NULL AUTO_INCREMENT,
  year date NULL,
  group_id varchar(3) NULL,
  benchmark float NULL,
  PRIMARY KEY (id)
)
;

-- -----------------------------------------------------
-- Table `calendar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS calendar;
CREATE TABLE calendar(
  id int NOT NULL AUTO_INCREMENT,
  date date NULL,
  start_time time NULL,
  end_time time NULL,
  shift int NULL,
  subject varchar(45) NULL,
  exam_id int NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_calender_exam1
    FOREIGN KEY (exam_id)
    REFERENCES exam (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
;

-- -----------------------------------------------------
-- Table `room`
-- -----------------------------------------------------
DROP TABLE IF EXISTS room;
CREATE TABLE room(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(45) NULL,
  location varchar(1000) NULL,
  calendar_id int NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_room_calendar1
    FOREIGN KEY (calendar_id)
    REFERENCES calendar (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
;



-- -----------------------------------------------------
-- Table `assignment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS assignment;
CREATE TABLE assignment(
  teacher_id int NOT NULL,
  room_id int NOT NULL,
  salary double NULL,
  position varchar(45) NULL,
  PRIMARY KEY (
    teacher_id,
    room_id
  ),
  CONSTRAINT fk_teacher_has_exam_teacher1
    FOREIGN KEY (teacher_id)
    REFERENCES teacher (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fk_assignment_room1
    FOREIGN KEY (room_id)
    REFERENCES room (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
;


-- -----------------------------------------------------
-- Table `transcript`
-- -----------------------------------------------------
DROP TABLE IF EXISTS transcript;
CREATE TABLE transcript(
  student_id varchar(45) NOT NULL,
  room_id int NOT NULL,
  score float NULL,
  PRIMARY KEY (
    student_id,
    room_id
  ),
  CONSTRAINT fk_transcript_student1
    FOREIGN KEY (student_id)
    REFERENCES student (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fk_transcript_room1
    FOREIGN KEY (room_id)
    REFERENCES room (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
;


-- -----------------------------------------------------
-- Table `support`
-- -----------------------------------------------------
DROP TABLE IF EXISTS support;
CREATE TABLE support(
  id int NOT NULL AUTO_INCREMENT,
  teacher_id int NOT NULL,
  answer TEXT NULL,
  question TEXT NULL,
  name varchar(45) NULL,
  phone varchar(50) NULL,
  id_number varchar(45) NULL,
  attachment varchar(255) NULL,
  date_question timestamp NULL,
  date_answer timestamp NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_post_teacher1
    FOREIGN KEY (teacher_id)
    REFERENCES teacher (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
;
