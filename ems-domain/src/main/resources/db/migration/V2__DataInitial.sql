INSERT INTO department (
  id,
  name
)
VALUES (
  '1',
  'Information Technology Facility'
);

INSERT INTO teacher (
  id,
  first_name,
  last_name,
  phone,
  address,
  department_id
)
VALUES (
  '1',
  'Admin',
  'Super',
  '0838354266',
  '227 Nguyen Van Cu, District 5, Ho Chi Minh City, Vietnam 70000',
  '1'
);

INSERT INTO user (
  login,
  password,
  teacher_id
)
VALUES (
  'admin',
  '$2a$10$nsMDLo/N/rdFvIZvEsl/HuuXQUE9Zu1HlMI2e6XqvNeAC/9yrIlcq',
  '1'
);

INSERT INTO role (
  id,
  name
)
VALUES (
  '1',
  'SUPER_USER'
);

INSERT INTO user_role_assignment (
  user_id,
  role_id
)
VALUES (
  'admin',
  '1'
)