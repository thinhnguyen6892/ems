package edu.hcmus.solutions.ems.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Exam extends IncrementIDBaseEntity {
    private Date year;
    private String groupId;
    private Double benchmark;

    @JsonIgnore
    @OneToMany(mappedBy = "exam")
    private List<Calendar> calendars;




    @Basic
    @Column(name = "year")
    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    @Basic
    @Column(name = "group_id")
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "benchmark")
    public Double getBenchmark() {
        return benchmark;
    }

    public void setBenchmark(Double benchmark) {
        this.benchmark = benchmark;
    }


    public List<Calendar> getCalendars() {
        return calendars;
    }

    public void setCalendars(List<Calendar> calendars) {
        this.calendars = calendars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exam exam = (Exam) o;
        return getId() == exam.getId() &&
                Objects.equals(year, exam.year) &&
                Objects.equals(groupId, exam.groupId) &&
                Objects.equals(benchmark, exam.benchmark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), year, groupId, benchmark);
    }

}
