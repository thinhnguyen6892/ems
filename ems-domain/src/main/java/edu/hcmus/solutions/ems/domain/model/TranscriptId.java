package edu.hcmus.solutions.ems.domain.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TranscriptId implements Serializable {

    private Student student;

    private Room room;


    public TranscriptId() {}

    public TranscriptId(
            Student student,
            Room room) {
        this.student = student;
        this.room = room;
    }

    //Getters omitted for brevity

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TranscriptId that = (TranscriptId) o;
        return Objects.equals(student, that.student) &&
                Objects.equals(room, that.room);
    }

    @Override
    public int hashCode() {
        return Objects.hash(student, room);
    }

    public String getStudent() {
        return student.getId();
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Long getRoom() {
        return room.getId();
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}