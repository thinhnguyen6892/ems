package edu.hcmus.solutions.ems.domain.repository;

import edu.hcmus.solutions.ems.domain.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    public List<Teacher> findAllByFirstNameLikeAndAddressEquals(String name, String add);
}
