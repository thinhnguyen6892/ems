package edu.hcmus.solutions.ems.domain.repository;

import edu.hcmus.solutions.ems.domain.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StudentRepository extends JpaRepository<Student, String> {

    Student findByIdAndIdNumber(String id, String idNumber);
}
