package edu.hcmus.solutions.ems.domain.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Room extends IncrementIDBaseEntity{
    private String name;
    private String location;

    @ManyToOne
    @JoinColumn(name = "calendar_id", referencedColumnName = "id", nullable = false)
    private Calendar calendar;

    @OneToMany(mappedBy = "room")
    private List<Assignment> assignments;


    @OneToMany(mappedBy = "room")
    private List<Transcript> transcripts;

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return getId() == room.getId() &&
                Objects.equals(name, room.name) &&
                Objects.equals(location, room.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), name, location);
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public List<Assignment> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<Assignment> assignments) {
        this.assignments = assignments;
    }

    public List<Transcript> getTranscripts() {
        return transcripts;
    }

    public void setTranscripts(List<Transcript> transcripts) {
        this.transcripts = transcripts;
    }
}
