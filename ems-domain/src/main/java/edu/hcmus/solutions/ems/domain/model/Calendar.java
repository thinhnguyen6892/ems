package edu.hcmus.solutions.ems.domain.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class Calendar extends IncrementIDBaseEntity{
    private Date date;
    private Date startTime;
    private Date endTime;
    private Integer shift;
    private String subject;

    @ManyToOne
    @JoinColumn(name = "exam_id", referencedColumnName = "id", nullable = false)
    private Exam exam;

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }
    @Basic
    @Column(name = "date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "start_time")
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time")
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "shift")
    public Integer getShift() {
        return shift;
    }

    public void setShift(Integer shift) {
        this.shift = shift;
    }

    @Basic
    @Column(name = "subject")
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Calendar calendar = (Calendar) o;
        return getId() == calendar.getId() &&
                Objects.equals(date, calendar.date) &&
                Objects.equals(startTime, calendar.startTime) &&
                Objects.equals(endTime, calendar.endTime) &&
                Objects.equals(shift, calendar.shift) &&
                Objects.equals(subject, calendar.subject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), date, startTime, endTime, shift, subject);
    }

}
