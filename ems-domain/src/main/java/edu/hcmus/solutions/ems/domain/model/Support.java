package edu.hcmus.solutions.ems.domain.model;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Objects;

@Entity
public class Support extends IncrementIDBaseEntity{
    private String answer;
    private String question;
    private String name;
    private String phone;
    private String idNumber;
    private String attachment;
    private Calendar dateQuestion;
    private Calendar dateAnswer;

    @ManyToOne
    @JoinColumn(name = "teacher_id", referencedColumnName = "id", nullable = false)
    private Teacher teacher;

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Basic
    @Column(name = "answer")
    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Basic
    @Column(name = "question")
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "id_number")
    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Basic
    @Column(name = "attachment")
    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_question")
    public Calendar getDateQuestion() {
        return dateQuestion;
    }

    public void setDateQuestion(Calendar dateQuestion) {
        this.dateQuestion = dateQuestion;
    }

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_answer")
    public Calendar getDateAnswer() {
        return dateAnswer;
    }

    public void setDateAnswer(Calendar dateAnswer) {
        this.dateAnswer = dateAnswer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Support support = (Support) o;
        return getId() == support.getId() &&
                Objects.equals(answer, support.answer) &&
                Objects.equals(question, support.question) &&
                Objects.equals(name, support.name) &&
                Objects.equals(phone, support.phone) &&
                Objects.equals(idNumber, support.idNumber) &&
                Objects.equals(attachment, support.attachment) &&
                Objects.equals(dateQuestion, support.dateQuestion) &&
                Objects.equals(dateAnswer, support.dateAnswer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), answer, question, name, phone, idNumber, attachment, dateQuestion, dateAnswer);
    }

}
