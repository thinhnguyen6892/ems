package edu.hcmus.solutions.ems.domain.repository;

import edu.hcmus.solutions.ems.domain.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
