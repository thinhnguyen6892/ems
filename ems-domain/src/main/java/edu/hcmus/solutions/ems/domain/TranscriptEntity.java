package edu.hcmus.solutions.ems.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "transcript", schema = "ems", catalog = "")
@IdClass(TranscriptEntityPK.class)
public class TranscriptEntity {
    private String studentId;
    private int roomId;
    private Double score;

    @Id
    @Column(name = "student_id")
    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Id
    @Column(name = "room_id")
    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    @Basic
    @Column(name = "score")
    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TranscriptEntity that = (TranscriptEntity) o;
        return roomId == that.roomId &&
                Objects.equals(studentId, that.studentId) &&
                Objects.equals(score, that.score);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId, roomId, score);
    }
}
