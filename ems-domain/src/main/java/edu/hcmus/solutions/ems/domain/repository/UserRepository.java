package edu.hcmus.solutions.ems.domain.repository;

import edu.hcmus.solutions.ems.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(String username);

    Optional<User> findOneByLogin(String login);
}
