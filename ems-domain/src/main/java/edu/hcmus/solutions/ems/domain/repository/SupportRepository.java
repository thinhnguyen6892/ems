package edu.hcmus.solutions.ems.domain.repository;

import edu.hcmus.solutions.ems.domain.model.Support;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupportRepository extends JpaRepository<Support, Long> {
}
