package edu.hcmus.solutions.ems.domain.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
public class Role extends IncrementIDBaseEntity{
    private String name;


    @ManyToMany(mappedBy = "role")
    private
    Set<User> assignedUser;

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return getId() == role.getId() &&
                Objects.equals(name, role.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), name);
    }

    public Set<User> getAssignedUser() {
        return assignedUser;
    }

    public void setAssignedUser(Set<User> assignedUser) {
        this.assignedUser = assignedUser;
    }
}
