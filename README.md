# Getting Started

## Configure your machine
*I will try to create Docker image to skip this step*
- Check installation of JDK > 1.8. pls refer here: https://drive.google.com/open?id=1IVsZmgp6LUxq1LiJCWZhMXz9DtfW4P6F
- Maven - follow this article https://www.mkyong.com/maven/how-to-install-maven-in-windows/
- Check MySQL installation (refer https://dev.mysql.com/downloads/installer/),  creates a database with the given name "ems" then create user: "admin" with password: "ThePassword" and grant full permissions to that user. Or you can change those information in "application.properties" under "ems-backoffice/src/main/resources"

## Preprocessor
- Build the project with Maven: Under project root (ems folder) open terminal (cmd) and types: mvn clean install

## Start application
- When build is finished run the command "java -jar .\ems-backoffice\target\ems-backoffice-0.0.1-SNAPSHOT.war".
- If no error the application will started at port 8080.
- Open your browser http://localhost:8080/

## Development
- This project support 2 ways to implement the requirement:
  1. Using Java web to create web application.
  2. Expose our data with secured Restful API and interact with another system.

## Infrastructure
Modularized structure: It has 5 modules depended on each other:
- backoffice: it is front controller (View)
- api: it is business logic interface which is interact directly with backoffice
- api-imp: it is business logic implementation. All of business logic will handled here. (Controller)
- domain: All definitions of entity can be found here. The application will communicate with database via this module. (Model)
- security: all security stuff stay in here.

## Test
Currently, It only have 1 page and 3 endpoints.
- Home Page: As you can see there nothing. :)
- Student Endpoint (/api/students): public endpoint everyone can access to this. It returns list of students
- User Endpoint (/api/users): secured endpoint. we need to login to access this endpoint and user who with role admin can access to this.
- Authentication Endpoint: (/api/authenticate): public endpoint - need to POST username and password to login our system.

## What's next?
