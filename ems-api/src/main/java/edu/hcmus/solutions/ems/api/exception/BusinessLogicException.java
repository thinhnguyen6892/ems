package edu.hcmus.solutions.ems.api.exception;

public class BusinessLogicException extends Exception {


    public BusinessLogicException(String message){
        super(message);
    }

}
