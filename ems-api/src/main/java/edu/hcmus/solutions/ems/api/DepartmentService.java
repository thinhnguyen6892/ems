package edu.hcmus.solutions.ems.api;

import edu.hcmus.solutions.ems.api.dto.DepartmentDto;
import edu.hcmus.solutions.ems.domain.model.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DepartmentService {

    Department findDepartment(Long DepartmentId);

    List<Department> findAll();

    Department createDepartment(DepartmentDto DepartmentDto);
    Department updateDepartment(DepartmentDto DepartmentDto);
    void deleteDepartment(DepartmentDto DepartmentDto);

}
