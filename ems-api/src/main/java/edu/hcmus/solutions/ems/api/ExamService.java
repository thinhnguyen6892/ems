package edu.hcmus.solutions.ems.api;

import edu.hcmus.solutions.ems.api.dto.ExamDto;
import edu.hcmus.solutions.ems.domain.model.Exam;

import java.util.List;

public interface ExamService {

    Exam findExam(Long ExamId);

    List<Exam> findAll();

    Exam createExam(ExamDto ExamDto);
    Exam updateExam(ExamDto ExamDto);

    void deleteExam(ExamDto ExamDto);
}
