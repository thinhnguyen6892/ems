package edu.hcmus.solutions.ems.api.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class RoomDto implements Serializable {
    private Long id;
    private String name;
    private String location;

    private CalendarDto calendar;

    private List<AssignmentDto> assignments;

    private List<TranscriptDto> transcripts;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomDto room = (RoomDto) o;
        return getId() == room.getId() &&
                Objects.equals(name, room.name) &&
                Objects.equals(location, room.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), name, location);
    }

    public CalendarDto getCalendar() {
        return calendar;
    }

    public void setCalendar(CalendarDto calendar) {
        this.calendar = calendar;
    }

    public List<AssignmentDto> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<AssignmentDto> assignments) {
        this.assignments = assignments;
    }

    public List<TranscriptDto> getTranscripts() {
        return transcripts;
    }

    public void setTranscripts(List<TranscriptDto> transcripts) {
        this.transcripts = transcripts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
