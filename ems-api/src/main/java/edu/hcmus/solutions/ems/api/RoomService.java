package edu.hcmus.solutions.ems.api;

import edu.hcmus.solutions.ems.api.dto.RoomDto;
import edu.hcmus.solutions.ems.domain.model.Room;

import java.util.List;

public interface RoomService {

    Room findRoom(Long RoomId);

    List<Room> findAll();

    Room createRoom(RoomDto RoomDto);
    Room updateRoom(RoomDto RoomDto);

    void deleteRoom(RoomDto RoomDto);
}
