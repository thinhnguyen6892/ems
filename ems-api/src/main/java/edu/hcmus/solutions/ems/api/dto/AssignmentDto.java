package edu.hcmus.solutions.ems.api.dto;

import java.io.Serializable;
import java.util.Objects;

public class AssignmentDto implements Serializable {

    private TeacherDto teacher;

    private RoomDto room;

    private Double salary;
    private String position;

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssignmentDto that = (AssignmentDto) o;
        return Objects.equals(teacher, that.teacher) &&
                Objects.equals(room, that.room) &&
                Objects.equals(salary, that.salary) &&
                Objects.equals(position, that.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teacher, room, salary, position);
    }

    public TeacherDto getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDto teacher) {
        this.teacher = teacher;
    }

    public RoomDto getRoom() {
        return room;
    }

    public void setRoom(RoomDto room) {
        this.room = room;
    }
}
