package edu.hcmus.solutions.ems.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import edu.hcmus.solutions.ems.api.util.CustomDateDeserializer;
import edu.hcmus.solutions.ems.api.util.CustomDateSerializer;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class StudentDto implements Serializable {
    private String id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String placeOfBirth;
    private String address;
    private String phone;
    private Integer region;
    private String idNumber;

    private RoomDto room;

    private Float score;

    private List<TranscriptDto> transcripts;

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getBirthDate() {
        return birthDate;
    }

    @JsonDeserialize(using = CustomDateDeserializer.class)
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }
    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getRegion() {
        return region;
    }
    public void setRegion(Integer region) {
        this.region = region;
    }

    public String getIdNumber() {
        return idNumber;
    }
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentDto student = (StudentDto) o;
        return Objects.equals(getId(), student.getId()) &&
                Objects.equals(firstName, student.firstName) &&
                Objects.equals(lastName, student.lastName) &&
                Objects.equals(birthDate, student.birthDate) &&
                Objects.equals(placeOfBirth, student.placeOfBirth) &&
                Objects.equals(address, student.address) &&
                Objects.equals(phone, student.phone) &&
                Objects.equals(region, student.region) &&
                Objects.equals(idNumber, student.idNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), firstName, lastName, birthDate, placeOfBirth, address, phone, region, idNumber);
    }

    public List<TranscriptDto> getTranscripts() {
        return transcripts;
    }

    public void setTranscripts(List<TranscriptDto> transcripts) {
        this.transcripts = transcripts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RoomDto getRoom() {
        return room;
    }

    public void setRoom(RoomDto room) {
        this.room = room;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }
}
