package edu.hcmus.solutions.ems.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class ExamDto implements Serializable {
    private Long id;
    private Date year;
    private String groupId;
    private Double benchmark;

    private List<CalendarDto> calendars;

    public Date getYear() {
        return year;
    }
    public void setYear(Date year) {
        this.year = year;
    }

    public String getGroupId() {
        return groupId;
    }
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Double getBenchmark() {
        return benchmark;
    }
    public void setBenchmark(Double benchmark) {
        this.benchmark = benchmark;
    }

    public List<CalendarDto> getCalendars() {
        return calendars;
    }
    public void setCalendars(List<CalendarDto> calendars) {
        this.calendars = calendars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExamDto exam = (ExamDto) o;
        return getId() == exam.getId() &&
                Objects.equals(year, exam.year) &&
                Objects.equals(groupId, exam.groupId) &&
                Objects.equals(benchmark, exam.benchmark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), year, groupId, benchmark);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
