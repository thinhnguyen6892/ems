package edu.hcmus.solutions.ems.api;

import edu.hcmus.solutions.ems.api.dto.StudentDto;
import edu.hcmus.solutions.ems.api.exception.BusinessLogicException;
import edu.hcmus.solutions.ems.domain.model.Student;

import java.util.List;

public interface StudentService {

    Student findStudent(String studentId);

    List<Student> findAll();

    Student createStudent(StudentDto studentDto) throws BusinessLogicException;

    Student findByIdAndIdNumber(String id, String idNumber) throws BusinessLogicException;
    Student adjustScore(StudentDto studentDto)throws BusinessLogicException;

}
