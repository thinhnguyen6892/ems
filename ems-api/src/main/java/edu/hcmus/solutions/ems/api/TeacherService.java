package edu.hcmus.solutions.ems.api;

import edu.hcmus.solutions.ems.api.dto.TeacherDto;
import edu.hcmus.solutions.ems.domain.model.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TeacherService {

    Teacher findTeacher(Long teacherId);

    List<Teacher> findAll();

    Teacher createTeacher(TeacherDto teacherDto);
    Teacher updateTeacher(TeacherDto TeacherDto);

    void deleteTeacher(TeacherDto TeacherDto);
}
