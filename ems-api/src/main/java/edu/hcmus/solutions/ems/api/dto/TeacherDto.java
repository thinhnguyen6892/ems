package edu.hcmus.solutions.ems.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import edu.hcmus.solutions.ems.domain.model.Teacher;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TeacherDto implements Serializable {
    private Long id;
    private String firstName;
    private String lastName;
    private String phone;
    private String address;

    public static TeacherDto base(Teacher teacher) {
        return new TeacherDto(teacher.getId(), teacher.getFirstName(), teacher.getFirstName(), teacher.getPhone(), teacher.getAddress());
    }

    public TeacherDto(Long id, String firstName, String lastName, String phone, String address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.address = address;
    }

    private DepartmentDto department;

    private List<UserDto> users;

    private List<SupportDto> supports;

    private List<AssignmentDto> assignments;

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        +.0++






















































































































































































































    this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public DepartmentDto getDepartment() {
        return department;
    }
    public void setDepartment(DepartmentDto department) {
        this.department = department;
    }

    public List<UserDto> getUsers() {
        return users;
    }
    public void setUsers(List<UserDto> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeacherDto teacher = (TeacherDto) o;
        return getId() == teacher.getId() &&
                Objects.equals(firstName, teacher.firstName) &&
                Objects.equals(lastName, teacher.lastName) &&
                Objects.equals(phone, teacher.phone) &&
                Objects.equals(address, teacher.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), firstName, lastName, phone, address);
    }

    public List<SupportDto> getSupports() {
        return supports;
    }

    public void setSupports(List<SupportDto> supports) {
        this.supports = supports;
    }

    public List<AssignmentDto> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<AssignmentDto> assignments) {
        this.assignments = assignments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
