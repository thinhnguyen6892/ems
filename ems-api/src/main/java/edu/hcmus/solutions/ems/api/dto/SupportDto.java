package edu.hcmus.solutions.ems.api.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;

public class SupportDto implements Serializable {
    private Long id;
    private String answer;
    private String question;
    private String name;
    private String phone;
    private String idNumber;
    private String attachment;
    private Calendar dateQuestion;
    private Calendar dateAnswer;

    private TeacherDto teacher;

    public TeacherDto getTeacher() {
        return teacher;
    }
    public void setTeacher(TeacherDto teacher) {
        this.teacher = teacher;
    }

    public String getAnswer() {
        return answer;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdNumber() {
        return idNumber;
    }
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getAttachment() {
        return attachment;
    }
    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Calendar getDateQuestion() {
        return dateQuestion;
    }
    public void setDateQuestion(Calendar dateQuestion) {
        this.dateQuestion = dateQuestion;
    }

    public Calendar getDateAnswer() {
        return dateAnswer;
    }
    public void setDateAnswer(Calendar dateAnswer) {
        this.dateAnswer = dateAnswer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SupportDto support = (SupportDto) o;
        return getId() == support.getId() &&
                Objects.equals(answer, support.answer) &&
                Objects.equals(question, support.question) &&
                Objects.equals(name, support.name) &&
                Objects.equals(phone, support.phone) &&
                Objects.equals(idNumber, support.idNumber) &&
                Objects.equals(attachment, support.attachment) &&
                Objects.equals(dateQuestion, support.dateQuestion) &&
                Objects.equals(dateAnswer, support.dateAnswer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), answer, question, name, phone, idNumber, attachment, dateQuestion, dateAnswer);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
