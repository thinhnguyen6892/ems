package edu.hcmus.solutions.ems.api;

import edu.hcmus.solutions.ems.api.dto.RoleDto;
import edu.hcmus.solutions.ems.api.dto.UserDto;
import edu.hcmus.solutions.ems.domain.model.Role;
import edu.hcmus.solutions.ems.domain.model.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> getUserWithAuthorities(String login);

    Optional<User> findOneByLogin(String login);

    List<UserDto> findAll();

    Role createRole(RoleDto roleDto);

    User assignRole(UserDto userDTO, RoleDto roleDto);

    User createUserInformation(UserDto userDTO, PasswordEncoder passwordEncoder);

}
