package edu.hcmus.solutions.ems.api.dto;


import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class CalendarDto implements Serializable {
    private Long id;
    private Date date;
    private Date startTime;
    private Date endTime;
    private Integer shift;
    private String subject;

    private ExamDto exam;
    public ExamDto getExam() {
        return exam;
    }

    public void setExam(ExamDto exam) {
        this.exam = exam;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public Date getStartTime() {
        return startTime;
    }
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    public Date getEndTime() {
        return endTime;
    }
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    public Integer getShift() {
        return shift;
    }
    public void setShift(Integer shift) {
        this.shift = shift;
    }
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalendarDto calendar = (CalendarDto) o;
        return getId() == calendar.getId() &&
                Objects.equals(date, calendar.date) &&
                Objects.equals(startTime, calendar.startTime) &&
                Objects.equals(endTime, calendar.endTime) &&
                Objects.equals(shift, calendar.shift) &&
                Objects.equals(subject, calendar.subject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), date, startTime, endTime, shift, subject);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
