package edu.hcmus.solutions.ems.api;

import edu.hcmus.solutions.ems.api.dto.CalendarDto;
import edu.hcmus.solutions.ems.domain.model.Calendar;

import java.util.List;

public interface CalendarService {

    Calendar findCalendar(Long CalendarId);

    List<Calendar> findAll();

    Calendar createCalendar(CalendarDto CalendarDto);
    Calendar updateCalendar(CalendarDto CalendarDto);

    void deleteCalendar(CalendarDto CalendarDto);
}
