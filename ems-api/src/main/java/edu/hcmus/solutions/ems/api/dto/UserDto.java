package edu.hcmus.solutions.ems.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import edu.hcmus.solutions.ems.domain.model.Teacher;
import edu.hcmus.solutions.ems.domain.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.Serializable;
import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto implements Serializable {


    public static UserDto teacherUser(User user){
        return new UserDto(user.getLogin(), user.getTeacher());
    }

    public UserDto(String login,  Teacher teacher) {
        this.login = login;
        this.teacher =  TeacherDto.base(teacher);
    }

    private String login;
    private String password;

    private TeacherDto teacher;

    private Boolean enabled;
    private List<RoleDto> role = new ArrayList<>();


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> result = new ArrayList<>();
        for(RoleDto userRoles : role){
            result.add(new SimpleGrantedAuthority(userRoles.getName()));
        }
        return result;
    }


    public List<RoleDto> getRole() {
        return role;
    }

    public void setRole(List<RoleDto> authorities) {
        this.role = authorities;
    }

    public String getUsername() {
        return login;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }


    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto user = (UserDto) o;
        return enabled == user.enabled &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(getUsername(), user.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, password, getUsername(), enabled);
    }

    public TeacherDto getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDto teacher) {
        this.teacher = teacher;
    }
}
