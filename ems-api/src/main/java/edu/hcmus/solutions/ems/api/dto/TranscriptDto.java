package edu.hcmus.solutions.ems.api.dto;

import java.io.Serializable;
import java.util.Objects;

public class TranscriptDto implements Serializable {

    private StudentDto student;

    private RoomDto room;

    private Float score;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TranscriptDto that = (TranscriptDto) o;
        return Objects.equals(student, that.student) &&
                Objects.equals(room, that.room) &&
                Objects.equals(score, that.score);
    }

    @Override
    public int hashCode() {
        return Objects.hash(student, room, score);
    }

    public RoomDto getRoom() {
        return room;
    }

    public void setRoom(RoomDto room) {
        this.room = room;
    }

    public StudentDto getStudent() {
        return student;
    }

    public void setStudent(StudentDto student) {
        this.student = student;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }
}
