package edu.hcmus.solutions.ems.api.impl;

import edu.hcmus.solutions.ems.api.DepartmentService;
import edu.hcmus.solutions.ems.api.dto.DepartmentDto;
import edu.hcmus.solutions.ems.domain.model.Department;
import edu.hcmus.solutions.ems.domain.model.Department;
import edu.hcmus.solutions.ems.domain.repository.DepartmentRepository;
import edu.hcmus.solutions.ems.domain.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    DepartmentRepository DepartmentRepository;


    @Override
    public Department findDepartment(Long DepartmentId) {
        return DepartmentRepository.getOne(DepartmentId);
    }

    @Override
    public List<Department> findAll() {
        return DepartmentRepository.findAll();
    }

    @Override
    public Department createDepartment(DepartmentDto DepartmentDto) {
        Department Department = new Department();
        Department.setName(DepartmentDto.getName());
        return DepartmentRepository.save(Department);
    }
    @Override
    public Department updateDepartment(DepartmentDto DepartmentDto) {
        Department Department = DepartmentRepository.getOne(DepartmentDto.getId());
        Department.setName(DepartmentDto.getName());
        return DepartmentRepository.save(Department);
    }
    @Override
    public void deleteDepartment(DepartmentDto DepartmentDto) {
        Department Department = DepartmentRepository.getOne(DepartmentDto.getId());

        DepartmentRepository.delete(Department);
    }

}
