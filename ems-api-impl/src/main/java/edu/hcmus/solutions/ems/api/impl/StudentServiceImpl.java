package edu.hcmus.solutions.ems.api.impl;

import edu.hcmus.solutions.ems.api.StudentService;
import edu.hcmus.solutions.ems.api.dto.RoomDto;
import edu.hcmus.solutions.ems.api.dto.StudentDto;
import edu.hcmus.solutions.ems.api.exception.BusinessLogicException;
import edu.hcmus.solutions.ems.domain.model.Room;
import edu.hcmus.solutions.ems.domain.model.Student;
import edu.hcmus.solutions.ems.domain.model.Transcript;
import edu.hcmus.solutions.ems.domain.model.TranscriptId;
import edu.hcmus.solutions.ems.domain.repository.RoomRepository;
import edu.hcmus.solutions.ems.domain.repository.StudentRepository;
import edu.hcmus.solutions.ems.domain.repository.TranscriptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    TranscriptRepository transcriptRepository;


    @Override
    public Student findStudent(String studentId) {
        return studentRepository.getOne(studentId);
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student createStudent(StudentDto studentDto) throws BusinessLogicException {
        Student student = studentRepository.getOne(studentDto.getId());
        if(student != null) throw new BusinessLogicException("Student is already exists");
        student = new Student();
        student.setId(studentDto.getId());
        student.setAddress(studentDto.getAddress());
        student.setBirthDate(studentDto.getBirthDate());
        student.setFirstName(studentDto.getFirstName());
        student.setLastName(studentDto.getLastName());
        student.setIdNumber(studentDto.getIdNumber());
        student.setRegion(studentDto.getRegion());
        student.setPhone(studentDto.getPhone());
        student.setPlaceOfBirth(studentDto.getPlaceOfBirth());
        Room room = roomRepository.getOne(studentDto.getRoom().getId());
        if(Objects.isNull(room)) throw new BusinessLogicException("Room is not exists");
        return studentRepository.save(student);
    }

    @Override
    public Student adjustScore(StudentDto studentDto)throws BusinessLogicException {
        Room room = roomRepository.getOne(studentDto.getRoom().getId());
        if(room == null) throw new BusinessLogicException("Room is not exists");
        Student student = studentRepository.getOne(studentDto.getId());
        if(student == null) {
            throw new BusinessLogicException("Student is not exists");
        }
        return studentRepository.save(student);

    }

    @Override
    public Student findByIdAndIdNumber(String id, String idNumber)throws BusinessLogicException {
        Student student = studentRepository.findByIdAndIdNumber(id, idNumber);
        if(student != null) return student;
        throw new BusinessLogicException("Student is not exists");
    }

}
