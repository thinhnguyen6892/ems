package edu.hcmus.solutions.ems.api.impl;

import edu.hcmus.solutions.ems.api.CalendarService;
import edu.hcmus.solutions.ems.api.dto.CalendarDto;
import edu.hcmus.solutions.ems.domain.model.Exam;
import edu.hcmus.solutions.ems.domain.model.Calendar;
import edu.hcmus.solutions.ems.domain.repository.ExamRepository;
import edu.hcmus.solutions.ems.domain.repository.CalendarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalendarServiceImpl implements CalendarService {

    @Autowired
    CalendarRepository CalendarRepository;

    @Autowired
    ExamRepository ExamRepository;

    @Override
    public Calendar findCalendar(Long CalendarId) {
        return CalendarRepository.getOne(CalendarId);
    }

    @Override
    public List<Calendar> findAll() {
        return CalendarRepository.findAll();
    }

    @Override
    public Calendar createCalendar(CalendarDto CalendarDto) {
        Exam Exam = ExamRepository.getOne(CalendarDto.getExam().getId());

        Calendar Calendar = new Calendar();
        Calendar.setDate(CalendarDto.getDate());
        Calendar.setStartTime(CalendarDto.getStartTime());
        Calendar.setEndTime(CalendarDto.getEndTime());
        Calendar.setShift(CalendarDto.getShift());
        Calendar.setSubject(CalendarDto.getSubject());
        Calendar.setExam(Exam);
        return CalendarRepository.save(Calendar);
    }
    @Override
    public Calendar updateCalendar(CalendarDto CalendarDto) {
        Exam Exam = ExamRepository.getOne(CalendarDto.getExam().getId());

        Calendar Calendar = CalendarRepository.getOne(CalendarDto.getId());
        Calendar.setDate(CalendarDto.getDate());
        Calendar.setStartTime(CalendarDto.getStartTime());
        Calendar.setEndTime(CalendarDto.getEndTime());
        Calendar.setShift(CalendarDto.getShift());
        Calendar.setSubject(CalendarDto.getSubject());
        Calendar.setExam(Exam);
        return CalendarRepository.save(Calendar);
    }
    @Override
    public void deleteCalendar(CalendarDto CalendarDto) {
        Calendar Calendar = CalendarRepository.getOne(CalendarDto.getId());

        CalendarRepository.delete(Calendar);
    }

}
