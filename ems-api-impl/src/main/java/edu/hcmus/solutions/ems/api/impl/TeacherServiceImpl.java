package edu.hcmus.solutions.ems.api.impl;

import edu.hcmus.solutions.ems.api.TeacherService;
import edu.hcmus.solutions.ems.api.dto.TeacherDto;
import edu.hcmus.solutions.ems.domain.model.Department;
import edu.hcmus.solutions.ems.domain.model.Teacher;
import edu.hcmus.solutions.ems.domain.repository.DepartmentRepository;
import edu.hcmus.solutions.ems.domain.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    TeacherRepository TeacherRepository;

    @Autowired
    DepartmentRepository DepartmentRepository;

    @Override
    public Teacher findTeacher(Long TeacherId) {
        return TeacherRepository.getOne(TeacherId);
    }

    @Override
    public List<Teacher> findAll() {
        return TeacherRepository.findAll();
    }

    @Override
    public Teacher createTeacher(TeacherDto TeacherDto) {
        Department department = DepartmentRepository.getOne(TeacherDto.getDepartment().getId());

        Teacher Teacher = new Teacher();
        Teacher.setAddress(TeacherDto.getAddress());
        Teacher.setFirstName(TeacherDto.getFirstName());
        Teacher.setLastName(TeacherDto.getLastName());
        Teacher.setPhone(TeacherDto.getPhone());
        Teacher.setDepartment(department);
        return TeacherRepository.save(Teacher);
    }
    @Override
    public Teacher updateTeacher(TeacherDto TeacherDto) {
        Department department = DepartmentRepository.getOne(TeacherDto.getDepartment().getId());

        Teacher Teacher = TeacherRepository.getOne(TeacherDto.getId());
        Teacher.setAddress(TeacherDto.getAddress());
        Teacher.setFirstName(TeacherDto.getFirstName());
        Teacher.setLastName(TeacherDto.getLastName());
        Teacher.setPhone(TeacherDto.getPhone());
        Teacher.setDepartment(department);
        return TeacherRepository.save(Teacher);
    }
    @Override
    public void deleteTeacher(TeacherDto TeacherDto) {
        Teacher Teacher = TeacherRepository.getOne(TeacherDto.getId());

        TeacherRepository.delete(Teacher);
    }

}
