package edu.hcmus.solutions.ems.api.impl;

import edu.hcmus.solutions.ems.api.UserService;
import edu.hcmus.solutions.ems.api.dto.RoleDto;
import edu.hcmus.solutions.ems.api.dto.UserDto;
import edu.hcmus.solutions.ems.domain.model.Department;
import edu.hcmus.solutions.ems.domain.model.Role;
import edu.hcmus.solutions.ems.domain.model.Teacher;
import edu.hcmus.solutions.ems.domain.model.User;
import edu.hcmus.solutions.ems.domain.repository.DepartmentRepository;
import edu.hcmus.solutions.ems.domain.repository.RoleRepository;
import edu.hcmus.solutions.ems.domain.repository.TeacherRepository;
import edu.hcmus.solutions.ems.domain.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    ModelMapper modelMapper;

    @Transactional(readOnly = true)
    @Override
    public Optional<User> getUserWithAuthorities(String login) {
        User currentUser = userRepository.findByLogin(login);
        currentUser.getAuthorities().size(); // eagerly load the association
        return Optional.of(currentUser);
    }

    @Override
    public Optional<User> findOneByLogin(String login) {
        return userRepository.findOneByLogin(login);
    }

    @Override
    public List<UserDto> findAll() {
        return userRepository.findAll().stream().map(user ->  UserDto.teacherUser(user)).collect(Collectors.toList());
    }

//    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    public Role createRole(RoleDto roleDto) {
        Role role = new Role();
        role.setName(roleDto.getName());
        return roleRepository.save(role);
    }

//    @PreAuthorize("hasRole('SUPER_ADMIN')")
    public User assignRole(UserDto userDTO, RoleDto roleDto) {
        Role userRole = roleRepository.getOne(roleDto.getId());
        User user = userRepository.getOne(userDTO.getLogin());
        user.addRole(userRole);
        return userRepository.save(user);
    }

//    @PreAuthorize("hasRole('SUPER_ADMIN')")
    public User createUserInformation(UserDto userDTO, PasswordEncoder passwordEncoder) {
        User newUser = new User();
        Long roleId = userDTO.getRole().get(0).getId();
        Role userRole = roleRepository.getOne(roleId);

        Teacher teacher = new Teacher();
        teacher.setFirstName(userDTO.getTeacher().getFirstName());
        teacher.setLastName(userDTO.getTeacher().getLastName());
        Department department = departmentRepository.getOne(userDTO.getTeacher().getDepartment().getId());
        teacher.setDepartment(department);
        teacher.setAddress(userDTO.getTeacher().getAddress());
        teacher.setPhone(userDTO.getTeacher().getPhone());
        teacherRepository.save(teacher);

        String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
        newUser.setLogin(userDTO.getLogin());
        newUser.setPassword(encryptedPassword);
        newUser.addRole(userRole);
        newUser.setTeacher(teacher);
        userRepository.save(newUser);
        User user = userRepository.getOne("admin");


        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }


}
