package edu.hcmus.solutions.ems.api.impl;

import edu.hcmus.solutions.ems.api.RoomService;
import edu.hcmus.solutions.ems.api.dto.RoomDto;
import edu.hcmus.solutions.ems.domain.model.Calendar;
import edu.hcmus.solutions.ems.domain.model.Room;
import edu.hcmus.solutions.ems.domain.repository.CalendarRepository;
import edu.hcmus.solutions.ems.domain.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    RoomRepository RoomRepository;

    @Autowired
    CalendarRepository CalendarRepository;

    @Override
    public Room findRoom(Long RoomId) {
        return RoomRepository.getOne(RoomId);
    }

    @Override
    public List<Room> findAll() {
        return RoomRepository.findAll();
    }

    @Override
    public Room createRoom(RoomDto RoomDto) {
        Calendar Calendar = CalendarRepository.getOne(RoomDto.getCalendar().getId());

        Room Room = new Room();
        Room.setName(RoomDto.getName());
        Room.setLocation(RoomDto.getLocation());
        Room.setCalendar(Calendar);
        return RoomRepository.save(Room);
    }
    @Override
    public Room updateRoom(RoomDto RoomDto) {
        Calendar Calendar = CalendarRepository.getOne(RoomDto.getCalendar().getId());

        Room Room = RoomRepository.getOne(RoomDto.getId());
        Room.setName(RoomDto.getName());
        Room.setLocation(RoomDto.getLocation());
        Room.setCalendar(Calendar);
        return RoomRepository.save(Room);
    }
    @Override
    public void deleteRoom(RoomDto RoomDto) {
        Room Room = RoomRepository.getOne(RoomDto.getId());

        RoomRepository.delete(Room);
    }

}
