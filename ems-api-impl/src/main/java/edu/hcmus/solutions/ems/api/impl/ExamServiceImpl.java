package edu.hcmus.solutions.ems.api.impl;

import edu.hcmus.solutions.ems.api.ExamService;
import edu.hcmus.solutions.ems.api.dto.ExamDto;
import edu.hcmus.solutions.ems.domain.model.Department;
import edu.hcmus.solutions.ems.domain.model.Exam;
import edu.hcmus.solutions.ems.domain.repository.DepartmentRepository;
import edu.hcmus.solutions.ems.domain.repository.ExamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamServiceImpl implements ExamService {

    @Autowired
    ExamRepository ExamRepository;


    @Override
    public Exam findExam(Long ExamId) {
        return ExamRepository.getOne(ExamId);
    }

    @Override
    public List<Exam> findAll() {
        return ExamRepository.findAll();
    }

    @Override
    public Exam createExam(ExamDto ExamDto) {

        Exam Exam = new Exam();
        Exam.setYear(ExamDto.getYear());
        Exam.setGroupId(ExamDto.getGroupId());
        Exam.setBenchmark(ExamDto.getBenchmark());
        return ExamRepository.save(Exam);
    }
    @Override
    public Exam updateExam(ExamDto ExamDto) {

        Exam Exam = ExamRepository.getOne(ExamDto.getId());
        Exam.setYear(ExamDto.getYear());
        Exam.setGroupId(ExamDto.getGroupId());
        Exam.setBenchmark(ExamDto.getBenchmark());
        return ExamRepository.save(Exam);
    }
    @Override
    public void deleteExam(ExamDto ExamDto) {
        Exam Exam = ExamRepository.getOne(ExamDto.getId());

        ExamRepository.delete(Exam);
    }

}
