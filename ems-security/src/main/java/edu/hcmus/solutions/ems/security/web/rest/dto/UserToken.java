package edu.hcmus.solutions.ems.security.web.rest.dto;

import edu.hcmus.solutions.ems.security.xauth.Token;

public class UserToken {
    Token token;
    UserDTO userDTO;

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
