package edu.hcmus.solutions.ems.security.xauth;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class XAuthConfiguration {


    private static final String SECRET = "al3$sEcr3212";
    private static final int TOKEN_VALIDITY_IN_SECONDS = 14400;

    @Bean
    public TokenProvider tokenProvider(){
        return new TokenProvider(SECRET, TOKEN_VALIDITY_IN_SECONDS);
    }

}
