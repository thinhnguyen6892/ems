package edu.hcmus.solutions.ems.security.web.rest;

import edu.hcmus.solutions.ems.api.dto.RoleDto;
import edu.hcmus.solutions.ems.api.dto.UserDto;
import edu.hcmus.solutions.ems.domain.model.Role;
import edu.hcmus.solutions.ems.domain.model.User;
import edu.hcmus.solutions.ems.security.Http401UnauthorizedEntryPoint;
import edu.hcmus.solutions.ems.security.web.rest.dto.UserDTO;
import edu.hcmus.solutions.ems.api.UserService;
import edu.hcmus.solutions.ems.security.web.rest.dto.UserToken;
import edu.hcmus.solutions.ems.security.xauth.TokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class AuthenticateController {

    private final Logger log = LoggerFactory.getLogger(AuthenticateController.class);
    private static final int PASS_EXPIRED_DAYS = 90;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    PasswordEncoder passwordEncoder;


    /**
     * POST  /register -> register the user.
     */
    @RequestMapping(value = "/createRole",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> createRole(@Valid @RequestBody RoleDto roleDto, HttpServletRequest request) {
        if(roleDto.getName() == null) {
            return new ResponseEntity<>("Role Name Should Be declare", HttpStatus.BAD_REQUEST);
        }
        Role role = userService.createRole(roleDto);
        if(role != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Role Name Should Be declare", HttpStatus.BAD_REQUEST);
    }


    /**
     * POST  /register -> register the user.
     */
    @RequestMapping(value = "/register",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> registerAccount(@Valid @RequestBody UserDto userDTO, HttpServletRequest request) {
        Assert.isTrue(userDTO.getAuthorities().size() == 1, "Exactly one role is supported");
        return userService.findOneByLogin(userDTO.getLogin())
                .map(user -> new ResponseEntity<>("login already in use", HttpStatus.BAD_REQUEST))
                .orElseGet(() -> {
                            User user;
                            try {
                                user = userService.createUserInformation(userDTO, passwordEncoder);
                                if(user != null) {
                                    return new ResponseEntity<>(HttpStatus.CREATED);
                                }
                            } catch (AccessDeniedException accessDeniedException) {
                                log.warn("Unhautorized access !", accessDeniedException);
                                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                            }
                            return new ResponseEntity<>(HttpStatus.CREATED);
                        }
                );
    }

    @RequestMapping(value = "/authenticate",
            method = RequestMethod.POST)
    public UserToken authorize(@RequestParam String username, @RequestParam String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = this.authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails details = this.userDetailsService.loadUserByUsername(username);
        Optional<User> userFromDatabase = userService.getUserWithAuthorities(username);

        // Passed authentication -> Verify password activeness
       /* LocalDate passwordLastModifiedDate;
        try {
            passwordLastModifiedDate = userService.getUserPasswordLastModifiedDate(user.getId());
        } catch (ArrayIndexOutOfBoundsException e) {
            passwordLastModifiedDate = user.getCreatedDate().toLocalDate();
        }
        long passwordActiveDays = ChronoUnit.DAYS.between(passwordLastModifiedDate, ZonedDateTime.now().toLocalDate());
        log.debug("Password active days: " + passwordActiveDays);
        if (passwordActiveDays > PASS_EXPIRED_DAYS) {
            // throw exception when password has been expired
            throw new HttpClientErrorException(HttpStatus.FORBIDDEN, "The user's password has been expired !");
        }*/
        User user = userFromDatabase.orElseThrow(() -> new HttpClientErrorException(HttpStatus.FORBIDDEN, "User " + username + " was not found in the database"));

        UserDTO userDTO = new UserDTO(
                user.getLogin(),
                "",
                user.getRole().stream().map(o -> o.getName()).collect(Collectors.toList()));

        UserToken userAndToken = new UserToken();
        userAndToken.setUserDTO(userDTO);
        userAndToken.setToken(tokenProvider.createToken(details));
        return userAndToken;
    }

}
