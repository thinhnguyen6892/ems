package edu.hcmus.solutions.ems.security.web.rest.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

public class UserDTO {
    @Pattern(regexp = "^[a-zA-Z0-9_]*$")
    @NotNull
    @Size(min = 1, max = 50)
    private String login;

    @Size(min = 5, max = 100)
    private String password;

    private List<String> roles;

    public UserDTO(String login, String password, List<String> roles) {
        this.login = login;
        this.password = password;
        this.roles = roles;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
